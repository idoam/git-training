#include <iostream>

namespace printer
{
    void display_args(int args_count, char *args[])
    {
        for (int i = 1; i < args_count; ++i)
            std::cout << "args[" << i << "] = '" << args[i] << "'\n";
    }

} // printer
