#!/bin/bash

VERBOSE="NO"
NUMBER="all"

while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    -n|--number)
      NUMBER="$2"
      shift # past argument
      shift # past value
      ;;
    -v|--verbose)
      VERBOSE="YES"
      shift # past argument
      ;;
    *)    # unknown option
      >&2 echo "unknown option \`"$1"'"
      exit 2
      ;;
  esac
done

echo "VERBOSE  = ${VERBOSE}"
echo "NUMBER   = ${NUMBER}"

tst() {
    # $1 function name
    # $2 progam flags
    # $3 arguments
    # $4 expected results

  ../build/main $2 $3 > "/tmp/tst.out"
  echo "$4" > "/tmp/expected.out"

  diff -u --color "/tmp/tst.out" "/tmp/expected.out"
  if [ $? -ne 0 ]
  then
    exit 2
  fi
}

tst "simple" "" "abc" "args[1] = 'abc'"

exit 0
